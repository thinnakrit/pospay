```
#!javascript

'use strict';

import React, { Component, PropTypes } from 'react';

import {
  StyleSheet,
  View,
  Text,
  Modal,
  BackAndroid,
  Platform
} from 'react-native';
import { Actions } from "react-native-router-flux";

import AppBg from '../Components/Control/AppBg';

import Shop from './Shop';
import Setting from './Setting';
import Search from './Search';
import MyProduct from './MyProduct';
import Board from './Board';
import AddShop from '../Components/Shop/AddShop';

class ModalComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  processModalReload(mode){
    this.props.processModalReload(mode);
  }
  
  render() {
    const state = this.props.state;
    let status = state.modal;
    let setComponent = <View />;

    if(state.modalComponent === 'shop'){
      setComponent = <Shop closeModal={()=>this.props.closeModal()} />;
    }else if(state.modalComponent === 'setting'){
      setComponent = <Setting closeModal={()=>this.props.closeModal()} />;
    }else if(state.modalComponent === 'search'){
      setComponent = <Search closeModal={()=>this.props.closeModal()}/>;
    }else if(state.modalComponent === 'board'){
      setComponent = <Board closeModal={()=>this.props.closeModal()}/>;
    }else if(state.modalComponent === 'addshop'){
      setComponent = (
        <AddShop 
          closeModal={()=>this.props.closeModal()}
        />
      );
    }else if(state.modalComponent === 'myproduct'){
      setComponent = <MyProduct closeModal={()=>this.props.closeModal()} processModalReload={(mode)=>this.processModalReload(mode)} />;
    }

  	// console.log('Test State : ' + JSON.stringify(this.props.state));
    return (
      <Modal
        transparent={true}
        visible={status}
        onRequestClose={() => ''}
      >
        <AppBg>
          {setComponent}
        </AppBg>
      </Modal>
    );
  }
}

export default ModalComponent;
```